#ifndef SOCKET_HPP
#define SOCKET_HPP

#include "array.hpp"
#include <string>
#include "network.hpp"
#include <iostream>

class socket{
	
	public:

	virtual void feedback();
	
	int conectar();
	void receberPacote(int endereco, array::array *enviarPacote);

};
#endif