#ifndef PACOTE_HPP
#define PACOTE_HPP

#include "array.hpp"
#include <iostream>

class pacote{

private:
	byte tag;//1 byte
	short int length;//2 bytes
	array::array *valuePacote;
	array::array *hash;

	array::array *enviar_pacote(byte tag, array::array *registrar_id);
	short swapBytes(short swapped);

public:

	pacote(byte tag);//tag vazio
	pacote(array::array *valuePacote);//cria pacote com value
	pacote(byte tag, array::array *valuePacote);// com tag e array

	~pacote();

	array::array static *registrar(int socket);
	array::array static *autenticar(int socket, array::array *chaveSimetrica);

};
#endif

