#include "network.hpp"
#include "array.hpp"
#include "crypto.hpp"
#include "pacote.hpp"
#include "socket.hpp"
#include "echo.hpp"

#include <iostream>

using namespace std;

int main(){

	int endereco;//endereco da conexao

	socket Socket;
	Socket.feedback();//uso e demonstracao de classe abstrata (com echo::feedback, vitual em hpp do socket)
	endereco = Socket.conectar();

	echo Echo;//heranca echo de socket
	Echo.feedback();//uso e demonstracao de classe abstrata
	Echo.conectarEcho();

	//registro - request registration
	array::array *recebido = pacote::registrar(endereco);

	//autenticacao
	pacote::autenticar(endereco,recebido);

	network::close(endereco);//encerrando conexao

	return 0;
}



