#include <iostream>
#include "socket.hpp"
#include "network.hpp"
#include <string>
#include "echo.hpp"

using namespace std;

void echo::feedback(){
	cout << endl << "\tConexao Echo" << endl << endl;
}

void echo::conectarEcho(){
	int endereco;

	cout << endl << "Conectando com o servidor...(3001)" << endl;

	if((endereco = network::connect("45.55.185.4",3001))<0){
		cout << "Conexao falhou no 3001! :(" << endl;
	}
	else{
		cout << "Conexao (endereco: " << endereco << ") no 3001 OK! :)" << endl;
	}
	
	
	
}