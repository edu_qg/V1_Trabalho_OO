#include "pacote.hpp"
#include "array.hpp"
#include "crypto.hpp"
#include <iostream>
#include "network.hpp"
#include <cstdio>

using namespace std;

union acessa_espaco{
	unsigned int tamanho;//4 bytes
	unsigned char os_bytes[4];//tb e quatro bytes posso acessar de qualquer jeito 
	//troca
};

array::array *pacote::enviar_pacote(byte tag, array::array *registrar_id){//tag, register_length, calcular hash
	//criando pacote para enviar
	array::array *enviar = array::create(4+3+registrar_id->length+20);
	
	union acessa_espaco aux3;
	aux3.tamanho = (unsigned int) (registrar_id->length + 3 + 20);

	registrar_id->data[0]=aux3.os_bytes[0];
	registrar_id->data[1]=aux3.os_bytes[1];

	union acessa_espaco aux2;
	aux2.tamanho = (unsigned int) registrar_id->length;
	
	registrar_id->data[4]=tag;
	registrar_id->data[5]=aux2.os_bytes[0];
	registrar_id->data[6]=aux2.os_bytes[1];

	memcpy(enviar->data+7, registrar_id->data, registrar_id->length);

	array::array *hash1 = array::create(20);
	hash1 = crypto::sha1(registrar_id);

	memcpy(enviar->data+7 + registrar_id->length, hash1->data,20);

	return enviar;
}

pacote::pacote(byte tag){
	this->tag = tag;
	this->length = 0;
}

pacote::pacote(array::array *valuePacote){
	tag = valuePacote->data[4];//o quinto valor manda a tag

	length = valuePacote->length-27;//4 header 20 hash 1 tag 2 length

	this->valuePacote = valuePacote;//usar array copy se nao funcionar

	hash=crypto::sha1(valuePacote);
}

pacote::pacote(byte tag, array::array *valuePacote){
	this->tag = tag;

	length = (valuePacote->data[1] & 0xFF) | (valuePacote->data[2] & 0xFF) << 8;

	this->valuePacote = valuePacote;

	hash = crypto::sha1(valuePacote);
}

pacote::~pacote(){

	//delete[];
}

array::array *pacote::autenticar(int socket, array::array *chaveSimetrica){
	
	return chaveSimetrica;

cout << "\tAutenticacao do cliente" << endl << endl;

}

array::array* pacote::registrar(int socket){

	cout << endl << "\tRegistro do cliente" << endl << endl;

	//RSA *chave_privada = crypto::rsa_read_private_key_from_PEM("doc/private.pem");
	RSA *chave_publica_servidor = crypto::rsa_read_public_key_from_PEM("doc/server_pk.pem");


	//Polimorfismo com array::create nessa linha e linha 134
	array::array *header = array::create(7);
	header->data[0]=3;
	header->data[1]=0;
	header->data[2]=0;
	header->data[3]=0;
	header->data[4]=0xC0;
	header->data[5]=0;
	header->data[6]=0;

	cout << endl << "Header: " << *header << endl << endl;

	network::write(socket,header);
	
	cout << "Socket: " << socket << endl;
	
	array::array *header_recebe = network::read(socket,4);

	union acessa_espaco aux;
	aux.os_bytes[0]=header_recebe->data[0];
	aux.os_bytes[1]=header_recebe->data[1];
	aux.os_bytes[2]=header_recebe->data[2];
	aux.os_bytes[3]=header_recebe->data[3];

	cout << endl << "Header recebe: " << *header_recebe << endl;

	cout << "Tamanho do union: " << aux.tamanho  << endl;

	array::array *recebe = network::read(socket,(size_t)aux.tamanho);

	array::destroy(header_recebe);

	cout << "Array data recebido: " << *recebe << endl;

	byte id[] = {0x73,0xaa,0x72,0x6c,0x96,0xf9,0xd5,0xcc};
	//Obrigado! O ID da tua aplicação é: 73 aa 72 6c 96 f9 d5 cc (hex)
	
	//visualiza ID
	cout << endl << "ID do cliente : " << endl;
	for(int i=0;i<8;i++){
		printf("%.2x ",id[i]);
	}
	cout << endl;
	
	//criando array para encriptar id
	array::array *id_enc  = array::create(8,id);

	id_enc = array::wrap(sizeof(id), id);

	//o cliente encripta de id com a chave publica (PK h) do servidor
	id_enc = crypto::rsa_encrypt(id_enc,chave_publica_servidor);

	array::array *registrarEncriptado = array::create(27+id_enc->length);
	registrarEncriptado->data[0] = 0x17;
	registrarEncriptado->data[1] = 0x02;
	registrarEncriptado->data[2] = 0;
	registrarEncriptado->data[3] = 0;
	registrarEncriptado->data[4] = 0xC2;
	registrarEncriptado->data[5] = 0x00;
	registrarEncriptado->data[6] = 0x02;

	memcpy(registrarEncriptado->data+7, id_enc->data, id_enc->length);

	array::array *hash2 = crypto::sha1(id_enc);

	memcpy(registrarEncriptado->data+7 + id_enc->length, hash2->data,20);

	cout << endl << "Pacote criado com sucesso!" << endl;
	cout << endl << "Enviando pacote..." << endl;
	network::write(socket,header);

	array::array *pacoteSim = network::read(socket);

	cout << endl << "Pacote Recebido com sucesso!"<< endl;
	cout << endl << "Pacote Recebido: " << *pacoteSim << endl;

	cout << endl << "Iniciando autenticacao..." << endl;

	array::destroy(hash2);

	return pacoteSim;
}
