#include "network.hpp"
#include "socket.hpp"
#include "array.hpp"

using namespace std;

 void socket::feedback(){
  	cout << endl << "\tConexao Socket" << endl << endl;
 }

int socket::conectar(){
	int endereco;

	cout << "Conectando com o servidor...(3000)" << endl;

	if((endereco = network::connect("45.55.185.4",3000))<0){
		cout << "Conexao falhou em 3000! :(" << endl;
	}
	else{
		cout << "Conexao (endereco: " << endereco << ") no 3000 OK! :)" << endl;
	}
	
	return endereco;
}

void socket::receberPacote(int endereco, array::array *enviarPacote){
	array::array *receberPacote;

	network::write(endereco, enviarPacote);

	if((receberPacote = network::read(endereco))== nullptr){
		cout << "Leitura do pacote falhou! :(" << endl;
	}else{
		cout << "Imprimindo o conteudo do pacote de " << receberPacote->length << " bytes recebido" << endl;
		for(int i=0;i<((int)receberPacote->length);i++){
			printf("%x ", receberPacote->data[i]);
		}
		printf("\n");
	}
	array::destroy(enviarPacote);


}